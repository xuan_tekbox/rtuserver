﻿///File: SGPProtocol.cs
/// Author: Xuan Lam
/// Product Version: TTU V1.0
/// Date: 05/23/2014
/// Brief:
/******************************************************************************	
    NameID	Date      	Feature ID	    Change Description
    ltXuan  04/28/2014  Server V0.0     Creat
    ltXuan  05/21/2014  Server V1.3     Add config file processing and SGP command/response parser
    ltXuan  05/23/2014  Server V1.4     Cleanup, and add data reception prcessing, data storage file
    ltXuan  05/29/2014  Server V0.B     Handling SGP config command for sensor measurement such as 0C!;0C1!;0C2!      
    ltXuan  06/23/2014  Server V1.11    Split functionalities into login, ConnectionSetup  and ParamConfig
    ltXuan  07/16/2014  Server V2.08    Add over-the-air firmware update feature (i.e. SGP command C,1,<filesize>)
 *  ltXuan  08/29/2014  Server V2.0D    Move enum's and other variables to TTUServerMain.cs, and make them constant global variables
 *  ltXuan  09/04/2014  Server V3.1     Add CSV file implementation (This feature is not fully implemented - and NOT TESTED). 
 *                                      But this build is otherwise stable.  Testing is GOOD
 *  ltXuan  09/05/2014  Server V3.2     Clean up for V3.1
 *  ltXuan  09/08/2014  Server V3.4     Fix .CSV file "Can't create, can't open" issue.
 *  ltXuan  09/09/2014  Server V3.5     Add support for RF (CPS) sensor parameter IDs (0x30, 0x35, 0x39)
 *                                      Code changed to support any parameter that matches the regular expression of the SGP command pattern
 *                                      Change logfile name format from log-ddMMHHmmss to ddMMyyyy-HHmmss
 *  ltXuan  10/31/2014  Server V3.6     Add more delay (10s) between sending C,1 command and the firmware update file for CPS devices because they need to clear their Flash memory first before accepting new firmware
 *                                      Increase delay between sending each OTA line from 100ms to 300ms
 *  ltXuan  11/22/2014  Server V3.7.1   1) Take out the .CSV support.  We should have a seperate program to parse logs into .CSV seperately.
 *                                      2) Refactor/rename:
 *                                          2.1 _sgpLogin_Confirm_with_KeepAliveTimeout() to _sgpLogin_Confirmed_SetToKeepConnectionAliveTimeout()
 *                                          2.2 _sgpLogin_Confirm_for_DataReception() to _sgpLogin_Confirmed_ReadyToReceiveData()
 *                                      3) Delete the "device.update" file.  Also, keep history of firmware update of each device
 *                                      4) Reduce the delay between each line of firmware update file from 300ms to 100ms
 *  ltXuan  12/02/2014  Server V3.7.2   Change the delay of C,1 response.  Delay 8s before sending the first line of response.
 *                                      Fix the path to the device.update.history
                             
 *******************************************************************************/
using System;
using System.Text;
using System.Collections;       //For ArrayList
using System.Threading;         //For Thread
using System.Net.Sockets;       //For Socket
using System.IO;                //For StreamWrite
using System.Text.RegularExpressions;
using System.Timers;
using System.Collections.ObjectModel;


namespace TTUServerTest
{
    class SGPProtocol : IProtocol
    {
        private Socket clntSock;            //Connection socket
        private ILogger consoleLogger;      //Logging facility
        private ILogger fileLogger;         //data output from client

        private byte[] msgBuffer;
        private byte[] _sentBuff;
        private byte[] _rcvdBuff;
        private int bytesRcvd;

        private string _username;
        private string _password;

        private string _cmdPart;
        private string _devID;
        private string _paramID;
        private string _paramSetting;
        private string _paramSettingResult;

        private string _datafilepath;
        private string _configfilepath;
        private string _devicefilepath;             //device.update file path
        private string _firmwarefilepath;
        //private string _csvfilepath;
        //private string _csvHeadline;
        //private string _fileType;

        /*******************************************************************************/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clntSock"></param>
        /// <param name="logger"></param>
        public SGPProtocol(Socket clntSock, ILogger logger)
        {
            this.clntSock = clntSock;
            this.consoleLogger = logger;

            msgBuffer = new byte[TTUServerMain.BUFSIZE];
            _sentBuff = new byte[TTUServerMain.BUFSIZE];
            _rcvdBuff = new byte[TTUServerMain.BUFSIZE];
        }
        /*******************************************************************************/
        /// <summary>
        /// 
        /// </summary>
        /// <param name="clientSock"></param>
        static void ConfigureTcpSocket(Socket clientSock)
        {
            try
            {
                // The socket will linger for 10 seconds after Socket.Close is called.
                clientSock.LingerState = new LingerOption(true, TTUServerMain.LINGER_STATE_TIMEOUT);

                // Disable the Nagle Algorithm for this tcp socket.
                //tcpSocket.NoDelay = true;

                // Set the receive buffer size to 8k
                clientSock.ReceiveBufferSize = TTUServerMain.DATA_BUFSIZE;    // 8192;

                // Set the timeout for synchronous receive methods to  
                // 30 seconds (30000 milliseconds.)
                clientSock.ReceiveTimeout = TTUServerMain.CONNECTION_TIMEOUT;

                // Set the send buffer size to 8k.
                clientSock.SendBufferSize = TTUServerMain.DATA_BUFSIZE;

                // Set the timeout for synchronous send methods 
                // to 30 second2 (30000 milliseconds.)			
                clientSock.SendTimeout = TTUServerMain.CONNECTION_TIMEOUT;
            }
            catch (SocketException se)
            {
                Console.WriteLine(DateTime.Now.ToString("yyyyMMddHH:mm:ss") + "TTU exception handling: ConfigureTcpSocket(): " + se.ErrorCode.ToString());
            }
        }
        /*******************************************************************************/
        public void handleClient()
        {
            ConfigureTcpSocket(clntSock);

            _datafilepath = TTUServerMain._dataDir;
            _configfilepath = TTUServerMain._configDir;
            _devicefilepath = TTUServerMain._deviceDir;
            _firmwarefilepath = TTUServerMain._deviceDir;


            //_csvfilepath = TTUServerMain._csvFileDir;
            //_csvHeadline = "";
            //_fileType = "";

            consoleLogger.writeText("Data file path: " + _datafilepath
                           + "\nConfig file path: " + _configfilepath
                           + "\nDevice file path: " + _devicefilepath);

            //_datafile = "log-" + DateTime.Now.ToString("ddMMHHmmss") + ".txt";

            consoleLogger.writeText("Thread " + Thread.CurrentThread.GetHashCode()
                + " - Handling the client<address:port>: " + clntSock.RemoteEndPoint);

            try
            {
                _handlingProtocol();

            }
            catch (SocketException se)
            {
                consoleLogger.writeText("TTU exception handling: " + se.ErrorCode + ": " + se.Message);
            }
            consoleLogger.writeText("TTU: Closing client socket <address:port>: " + clntSock.RemoteEndPoint);

            //Clean up this client socket object
            clntSock.Close();

        }

        /*******************************************************************************/
        private void _ttuLog(ILogger log, string msg)
        {
            if (log != null)
            {
                log.writeText(DateTime.Now.ToString("dd-MM-yy HH:mm:ss") + msg);
            }
        }
        /*******************************************************************************/
        private bool _transmit(string msg)
        {
            bool ret = true;
            string pat_cmd = @"\G(.+)";
            msg += "\r\n";
            try
            {
                msgBuffer = Encoding.ASCII.GetBytes(msg);
                clntSock.Send(msgBuffer, 0, msg.Length, SocketFlags.None);
            }
            catch
            {
                consoleLogger.writeText("Catch all exception in _transmit().");
                ret = false;
            }

            if (ret)
            {
                foreach (Match m in Regex.Matches(msg, pat_cmd))
                {
                    _ttuLog(consoleLogger, " ->-> " + m.Groups[1].Value);
                    _ttuLog(fileLogger, " ->-> " + m.Groups[1].Value);
                }
            }
            return ret;
        }
        /*******************************************************************************/
        private TTUServerMain.RESULT_STATUS _receive(TTUServerMain.CmdCode cmdCode) //(RespState respState)
        {
            String msg = "";
            String temp = "";
            int i;
            //string pat_KSLine = @"^([KS],)(\w{1,4}),([\w\W]+)";
            //string pat_DLine = @"^(D,)(\d+),(\w{1,4}),([\w\W]+)";
            string pat_ALine = @"^(A,)(\w{1,4}),([\w\W]+)";
            string pat_r5 = @"^(R,5),([A-Fa-f0-9]{1,9}),([+-]?\d{1})";         //Ex. R,5,2710,1          
            string pat_r3 = @"^(R,3),([A-Fa-f0-9]{1,9}),([+-]?\d{1}),([A-Fa-f0-9]{1,2})";   //Ex. R,3,2713,1,20
            //string pat_r2 = @"^(R,2),([A-Fa-f0-9]{1,4}),([+-]?\d{1}),([A-Fa-f0-9]{1,2})(,?)([\W\w:;]+)";   //Ex. R,2,2713,1,20,3C or R,2,2713,1,2,2000010100:01:47  or R,2,271F,1,43,0M1;1M1

            //string pat_r2_ok = @"^(R,2,[A-Fa-f0-9]{1,9},1),([A-Fa-f0-9]{1,2}),([\W\w:;]+)(\r\n)"; //,([\w:;]+)";
            string pat_r2_ok = @"^(R,2,[A-Fa-f0-9]{1,9},1),([A-Fa-f0-9]{1,2}),([\w:;]*)"; //,([\w:;]+)";

            string pat_r1 = @"^(R,1)";
            string pat_user = @"^([A-Fa-f0-9]{4,10})";
            string pat_pass = @"^([A-Fa-f0-9]{4,10})";


            string pat_resp = @"\G(.+)\r\n";        //Grab everything in the response up to \r\n

            string paramID = "";
            string paramSetting = "";
            string paramSettingResult = "";
            Match match;
            TTUServerMain.RESULT_STATUS result = TTUServerMain.RESULT_STATUS.RESP_RCVD_CONTINUE;

            while (result == TTUServerMain.RESULT_STATUS.RESP_RCVD_CONTINUE)
            {
                if ((bytesRcvd = clntSock.Receive(msgBuffer, 0, msgBuffer.Length, SocketFlags.None)) == 0)
                {
                    consoleLogger.writeText("No more data from"
                                      + clntSock.RemoteEndPoint.ToString()
                                      + ".  Close connection!");
                    break;
                }

                temp = Encoding.ASCII.GetString(msgBuffer, 0, bytesRcvd);
                msg += temp;

                if (temp.IndexOf('\n') < 0)
                    continue;

                i = msg.IndexOf('\n');
                while (i >= 0)
                {
                    temp = msg.Substring(i + 1);    //referencing to the sub string after '\n'
                    msg = msg.Substring(0, i + 1);  //re-referencing to the sub-string before '\n'

                    match = Regex.Match(msg, pat_resp, RegexOptions.IgnoreCase);
                    if (match.Success)
                    {
                        msg = match.Groups[1].Value;
                        _ttuLog(consoleLogger, " <-<- " + msg);
                        _ttuLog(fileLogger, " <-<- " + msg);

                    }
                    switch (cmdCode)
                    {
                        case TTUServerMain.CmdCode.CMD_1:
                            //if (Regex.IsMatch(msg, pat_KSLine) || Regex.IsMatch(msg, pat_DLine))
                            //{
                            //    result = TTUServerMain.TTUServerMain.RESULT_STATUS.RESP_RCVD_CONTINUE;
                            //}
                            //else 
                            if (Regex.IsMatch(msg, pat_ALine))
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_OK;
                            }
                            else
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_CONTINUE;
                            }
                            break;
                        case TTUServerMain.CmdCode.CMD_USERNAME:
                            match = Regex.Match(msg, pat_user, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_OK;
                                _username = match.Groups[1].Value;
                            }
                            else result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
                            break;
                        case TTUServerMain.CmdCode.CMD_PASSWORD:
                            match = Regex.Match(msg, pat_pass, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_OK;
                                _password = match.Groups[1].Value;
                            }
                            else result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;

                            break;

                        case TTUServerMain.CmdCode.CMD_C1:
                            match = Regex.Match(msg, pat_r1, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_OK;
                            }
                            else result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
                            break;
                        case TTUServerMain.CmdCode.CMD_C2:

                            match = Regex.Match(msg, pat_r2_ok, RegexOptions.IgnoreCase);

                            if (match.Success)
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_OK;
                                _paramID = match.Groups[2].Value;
                                _paramSetting = match.Groups[3].Value;
                            }
                            else
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
                            }
                            break;
                        case TTUServerMain.CmdCode.CMD_C3:
                            match = Regex.Match(msg, pat_r3, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_OK;

                                _cmdPart = match.Groups[1].Value;
                                _devID = match.Groups[2].Value;

                                _paramSetting = "";     //NOT USED
                                _paramSettingResult = paramSettingResult = match.Groups[3].Value;

                                _paramID = paramID = match.Groups[4].Value;

                            }
                            else result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
                            break;
                        case TTUServerMain.CmdCode.CMD_C5:       //Keep connection alive-timeout
                            match = Regex.Match(msg, pat_r5, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                result = TTUServerMain.RESULT_STATUS.RESP_RCVD_OK;
                                paramSetting = match.Groups[3].Value;
                            }
                            else result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
                            break;
                        default:


                            result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
                            break;
                    }

                    msg = temp;
                    i = msg.IndexOf('\n');

                } //Check if there is another '\n' in the message msg

            }
            return result;
        }

        /*******************************************************************************/
        //bool _sgpLogin_Confirm_for_DataReception()
        bool _sgpLogin_Confirmed_ReadyToReceiveData()
        {
            TTUServerMain.RESULT_STATUS resp = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
            string cmd = TTUServerMain._CMD_1_LOGIN_OK_FOR_DATA;
            _transmit(cmd);
            resp = _receive(TTUServerMain.CmdCode.CMD_1);             //Receiving data from TTU device 
            if (resp == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK)     //receiving the "ALINE" of the response
            {
                cmd = TTUServerMain._CMD_B_FOR_SETTING_PARAM;
                _transmit(cmd);

                return true;
            }
            return false;
        }
        /*******************************************************************************/
        //bool _sgpLogin_Confirm_with_KeepAliveTimeout()
        bool _sgpLogin_Confirmed_SetToKeepConnectionAliveTimeout(string keepAliveTimeout)
        {
            TTUServerMain.RESULT_STATUS resp = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;
            string cmd = TTUServerMain._CMD_2_LOGIN_OK_FOR_SETUP;
            _transmit(cmd);             //Send SGP '2' command, will follow by more SGP commands.

            cmd = TTUServerMain._CMD_C5_KEEP_CONNECTION_ALIVE_TIME + keepAliveTimeout;
            _transmit(cmd);             //transmit SGP command C,5
            resp = _receive(TTUServerMain.CmdCode.CMD_C5);

            if (resp == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK)
                return true;

            return false;
        }
        /*******************************************************************************/
        bool _sgpLogin()
        {
            TTUServerMain.RESULT_STATUS result = TTUServerMain.RESULT_STATUS.RESP_RCVD_ERROR;

            _transmit(TTUServerMain._CMD_USERNAME);
            result = _receive(TTUServerMain.CmdCode.CMD_USERNAME);

            if (result == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK)
            {
                _transmit(TTUServerMain._CMD_PASSWORD);
                result = _receive(TTUServerMain.CmdCode.CMD_PASSWORD);
                if (result == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK)
                    return true;
            }
            return false;
        }

        /*******************************************************************************/
        bool _isFirmwareUpdateNeeded(string deviceId)
        {
            bool ret = false;
            string updateHistoryFile = _devicefilepath + @"\device.update.history";
            _devicefilepath += @"\device.update";
            
                
            if (!File.Exists(_devicefilepath))
                return ret;
            try
            {
                using (StreamReader sr = new StreamReader(_devicefilepath))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        if (line.StartsWith(deviceId))  //TODO: Need to check db for the device deployed status
                        {
                            ret = true;
                            break;
                        }
                    }
                    sr.Close();

                    //TODO: Implement a DB to set device update_needed = false
                    //Delete the "device.update" file.  Also, keep history of firmware update of each device
                    if (ret == true)
                    {
                        File.AppendAllText(updateHistoryFile, DateTime.Now.ToString("ddMMyyyy-HHmmss")+ " - updated: " + deviceId);
                        
                        File.Delete(_devicefilepath);
                    }
                }
            }
            catch (IOException ioex)
            {
                consoleLogger.writeText("TTU exception handling: " + ioex.GetHashCode() + ".  Can't open this file");
            }
            return ret;
        }
        /*******************************************************************************/
        bool _updateFirmware(string deviceId)
        {
            bool ret = false;
            string line;
            _firmwarefilepath += @"\fwUpdate.hex";

            if (!File.Exists(_firmwarefilepath))
                return ret;

            FileInfo fi = new FileInfo(_firmwarefilepath);

            string cmd = TTUServerMain._CMD_C1_FW_UPDATE + Convert.ToString(fi.Length);
            if (_transmit(cmd))
            {

                //Thread.Sleep(10000);    //Xuan-modified 11/22/2014
                Thread.Sleep(8000);         //Xuan-modified 
            }

            try
            {
                using (StreamReader sr = new StreamReader(_firmwarefilepath))
                {
                    while ((line = sr.ReadLine()) != null)
                    {
                        ret = false;
                        if (ret = _transmit(line))
                        {
                            
                            Thread.Sleep(100);      //Xuan-modified 11/22/2014
                            //Thread.Sleep(300);    //Xuan-modified 10/31/2014
                            continue;
                        }
                        else
                            break;
                    }                    
                    sr.Close();

                }
            }
            catch (IOException ioex)
            {
                consoleLogger.writeText("TTU exception handling: " + ioex.GetHashCode() + ".  Can't open this file");
            }
            return ret;
        }

        /*******************************************************************************/
        ///// <summary>
        /// bool _sgpSetupFile()
        /// </summary>
        /// <returns></returns>
        bool _sgpSetupFile(string userName)//, string dataFileName)
        {
            bool ret = true;
            try
            {
                //create a config filepath and filename for this device
                _configfilepath += @"\" + userName + ".cfg";
                consoleLogger.writeText("Finding a config file for this device..." + _configfilepath);
                if (!File.Exists(_configfilepath))
                {
                    File.Copy(TTUServerMain._defaultConfigFile, _configfilepath);
                }

                //create a data log filepath for this device
                _datafilepath += @"\" + userName;

                if (!Directory.Exists(_datafilepath))
                {
                    Directory.CreateDirectory(_datafilepath);
                }
                //_datafilepath += @"\" + "log-" + DateTime.Now.ToString("ddMMHHmmss") + ".txt";
                _datafilepath += @"\" + DateTime.Now.ToString("ddMMyyyy-HHmmss") + ".txt";
                if (!File.Exists(_datafilepath))
                {
                    consoleLogger.writeText("Creating a datalog file: " + _datafilepath);
                    fileLogger = new FileLogger(_datafilepath);
                }
            }
            catch
            {
                ret = false;
                consoleLogger.writeText("Exception in _sgpSetupFile().");
            }
            return ret;
        }
        /*******************************************************************************/
        /// <summary>
        /// Query and change device's date/time if it's not synchronized 
        /// with the date/time in the timezone where the device is located.
        /// 
        /// </summary>
        /// <param name="paramID">param ID indicated date/time paramer</param>
        /// <param name="targetLocalTime">the date/time in the timezone where the device is located.
        /// If this targeLocalTIme is empty, then server's date/time is applied </param>
        /// <returns></returns>
        bool HandleSystemDateTime(string paramID, string targetLocalTime)
        {
            bool ret = false;
            TTUServerMain.RESULT_STATUS resp;
            string paramSetting = targetLocalTime;
            string cmd = TTUServerMain._CMD_C2_READ_PARAM + paramID;

            if (paramSetting.Equals(""))
            {
                paramSetting = DateTime.Now.ToString("yyyyMMddHH:mm:ss");     //will set device's date/time to the server's date/time
            }

            _transmit(cmd);     //query current device's date/time
            resp = _receive(TTUServerMain.CmdCode.CMD_C2);

            if (resp == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK
                && paramID.Equals(_paramID)     //&& String.Compare(paramID, _paramID) == 0 
                && String.Compare(paramSetting, 0, _paramSetting, 0, "yyyyMMddHH:mm".Length) != 0)   //!paramSetting.Equals(_paramSetting) )     
            {
                cmd = TTUServerMain._CMD_C3_WRITE_PARAM + paramID + "," + paramSetting;
                _transmit(cmd);     //change device's date/time to the local date/time of device's timezone

                resp = _receive(TTUServerMain.CmdCode.CMD_C3);
                if (resp == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK)
                    ret = true;
            }
            return ret;
        }
        /*******************************************************************************/
        bool _sgpTerminateConnection()
        {
            string cmd = TTUServerMain._CMD_C0_END_CONNECTION;
            _transmit(cmd);

            _ttuLog(consoleLogger, " -> " + "EOF");
            _ttuLog(fileLogger, " -> " + "EOF");

            return true;
        }
        /*******************************************************************************/
        bool HandleDeviceParamSetting(string paramID, string paramSetting)
        {
            bool ret = false;
            TTUServerMain.RESULT_STATUS resp;
            string cmd = "";
            cmd = TTUServerMain._CMD_C2_READ_PARAM + paramID;
            _transmit(cmd);

            resp = _receive(TTUServerMain.CmdCode.CMD_C2);

            if (resp == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK
                && paramID.Equals(_paramID, StringComparison.OrdinalIgnoreCase)     //&& String.Compare(paramID, _paramID) == 0
                && !paramSetting.Equals(_paramSetting, StringComparison.OrdinalIgnoreCase))     //&& String.Compare(paramSetting, _paramSetting) != 0)
            {
                cmd = TTUServerMain._CMD_C3_WRITE_PARAM + paramID + "," + paramSetting;
                _transmit(cmd);

                resp = _receive(TTUServerMain.CmdCode.CMD_C3);
                if (resp == TTUServerMain.RESULT_STATUS.RESP_RCVD_OK)
                    ret = true;
            }
            return ret;
        }
        /*******************************************************************************/
        private bool processDeviceParamSetting(string line)
        {
            bool ret = false;
            string input_pat = @"^([A-Fa-f0-9]{1,2}),([\w\s\.,;'-]+)";
            string timezone_pat = @"(')([\w\s]+)(')";       //'' or 'Pacific Time'
            //string csvformat_pat = @"(\.csv);([\w\s,-]+)";  //".csv" and "Date,Time,Air Temp,SM-010cm,SM-020cm"
            string param_pat = @"([\w\.-:;]+)";                 //"xuanttu.tekbox.net" or "m-i090" or "2000010100:00:00"    
            Match match;
            string paramID;
            string paramSetting = "";

            match = Regex.Match(line, input_pat, RegexOptions.IgnoreCase);

            
            if (match.Success)
            {
                paramID = match.Groups[1].Value;
                paramSetting = match.Groups[2].Value;

                ret = true;
                switch (paramID)   //Match for valid parameter
                {
                    //.CSV file format support
                    //case TTUServerMain._PARAMID_FOR_FILEFORMAT:             //"E0"
                    //    //_csvHeadline = paramSetting;
                    //    match = Regex.Match(paramSetting, csvformat_pat, RegexOptions.IgnoreCase);
                    //    if (match.Success)
                    //    {
                    //        _fileType = match.Groups[1].Value;
                    //        _csvHeadline = match.Groups[2].Value;
                    //    }
                    //    else
                    //    {
                    //        _fileType = "";
                    //        _csvHeadline = "";
                    //    }
                    //    break;
                    case TTUServerMain._PARAMID_FOR_DATETIME:                 //"2"
                        if (!paramSetting.Equals(@"''"))                        //'' NOT an empty timezone string
                        {
                            match = Regex.Match(paramSetting, timezone_pat, RegexOptions.IgnoreCase);
                            if (match.Success)
                            {
                                paramSetting = _getTimeByDisplayName(match.Groups[2].Value);       //compute time by timezone setting such as 'Adelaide' or 'Pacific Time'
                            }
                            else
                                paramSetting = "";
                        }
                        else
                        {
                            paramSetting = "";
                        }
                        HandleSystemDateTime(paramID, paramSetting);
                        break;
                    //case TTUServerMain._PARAMID_FOR_TRANSMISSION_INTERVAL:    //"20"
                    //case TTUServerMain._PARAMID_FOR_MEASUREMENT_INTERVAL:     //"40"
                    //case TTUServerMain._PARAMID_FOR_MEASUREMENT_CMD_LIST:     //"43"
                    //case TTUServerMain._PARAMID_FOR_SENSOR_ADDRESS_LIST:      //"45"
                    //    HandleDeviceParamSetting(paramID, paramSetting);
                        //break;
                    default:    //Support any parameter ID and settings that match the "param_pat" above 
                        match = Regex.Match(paramSetting, param_pat, RegexOptions.IgnoreCase);
                        if (match.Success)
                        {
                            paramSetting = match.Groups[1].Value;       //compute time by timezone setting such as 'Adelaide' or 'Pacific Time'
                            ret = HandleDeviceParamSetting(paramID, paramSetting);
                        }
                        else
                        {
                            paramSetting = "";
                            ret = false;
                        }
                        break;
                }
            }
            return ret;
        }
        /*******************************************************************************/
        /// <summary>
        /// Determines the date/time by the specified timezone displayname
        /// </summary>
        /// <param name="tzDisplayName">timezone display name.  Example: Adelaide, Pacific Time, Hanoi, etc.</param>
        /// <returns>current date/time of the timezone of specified displayname</returns>
        string _getTimeByDisplayName(string tzDisplayName)
        {
            string targetLocalTime = "";
            ReadOnlyCollection<TimeZoneInfo> zones = TimeZoneInfo.GetSystemTimeZones();

            foreach (TimeZoneInfo zone in zones)
            {
                if (zone.DisplayName.Contains(tzDisplayName))
                {
                    targetLocalTime =
                        TimeZoneInfo.ConvertTimeBySystemTimeZoneId(DateTime.Now,
                        TimeZoneInfo.Local.Id,
                        zone.Id).ToString("yyyyMMddHH:mm:ss");
                    break;
                }
            }
            return targetLocalTime;
        }

        /*******************************************************************************/
        /// <summary>
        ///  This method opens a TTU configuration file, reads each line in it, parse it, and 
        ///  sends that line to the connected TTU client
        /// </summary>

        void _handlingProtocol()
        {
            string line = "";
            //_fileType = "";

            if (_sgpLogin() && _sgpSetupFile(_username))//, _datafile))
            {
                if (_sgpLogin_Confirmed_SetToKeepConnectionAliveTimeout(TTUServerMain._KEEPCONNECTIONALIVETIME) 
                    && _sgpLogin_Confirmed_ReadyToReceiveData())
                {
                    if (_isFirmwareUpdateNeeded(_username) ) //_username == deviceID
                    {
                        consoleLogger.writeText("Firmware OTA update in progress...");
                        if (_updateFirmware(_username))
                        {
                            consoleLogger.writeText("Successfully downloading firmware");

                        }
                        else
                        {
                            consoleLogger.writeText("Failed downloading firmware");
                        }
                    }
                    else //Commands and Responses for parameter settings
                    {
                        try
                        {
                            using (StreamReader sr = new StreamReader(_configfilepath))
                            {
                                consoleLogger.writeText("Debug: Open a configuration file..." + _configfilepath);
                                //consoleLogger.writeText(_configfilepath);

                                while ((line = sr.ReadLine()) != null)
                                {
                                    //consoleLogger.writeText("Debug: \"" + line + "\"");
                                    processDeviceParamSetting(line);
                                }
                            }
                        }
                        catch (IOException ioex)
                        {
                            consoleLogger.writeText("TTU exception handling: " + ioex.GetHashCode() + ".  Can't open this file");
                        }
                    }
                }

                if (_sgpTerminateConnection())
                {
                    //Close datalogfile
                    try
                    {
                        fileLogger.writeClose();    //close the data logfile
                        //if (!_fileType.Equals(""))
                        //{
                        //    exportCSVFile();
                        //}

                    }
                    catch
                    {
                        consoleLogger.writeText("Catch all exception when closing logfile().");
                    }
                }
            }
            else
            {
                consoleLogger.writeText("ERROR: Login failed.");
            }
        }
        /*******************************************************************************/
    }
}
