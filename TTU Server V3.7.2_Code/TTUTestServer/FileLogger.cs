﻿using System;           //For String
using System.Collections;   //For ArrayList
using System.IO;            //For StreamWrite
using System.Threading;     //For Mutex


namespace TTUServerTest
{
    class FileLogger : ILogger
    {
        private static Mutex mutex = new Mutex();
        private StreamWriter output;    //Log file

        /// <summary>
        /// To create an instance of FileLogger with StreamWriter "output".
        /// StreamWrite with the param boolean=true - to create and append to this file.  
        /// StreamWrite with the param boolean=false - to create and overwrite to this file
        /// If the file exists, it can be either overwritten or appended to.
        /// </summary>
        /// <param name="filename"></param>
        public FileLogger(String filename)
        {
            //Create log file
            output = new StreamWriter(filename, true);          
        }

        public void writeEntry(ArrayList entry)
        {
            mutex.WaitOne();
            IEnumerator line = entry.GetEnumerator();

            while (line.MoveNext())
            {
                output.WriteLine(line.Current);
            }

            //output.WriteLine();
            output.Flush();

            mutex.ReleaseMutex();
        }

        public void writeText(String text)
        {
            mutex.WaitOne();

            output.WriteLine(text);
            //output.WriteLine();
            output.Flush();

            mutex.ReleaseMutex();
        }

        public void writeClose()
        {
                output.Close();
        }
    }
}
