﻿///File: TTUServerMain.cs
/// Author: Xuan Lam
/// Product Version: TTU V1.0
/// Date: 05/23/2014
/// Brief:
/******************************************************************************	
    NameID	Date      	Feature ID	    Change Description
    ltXuan  04/28/2014  Server V0.0     Creat
    ltXuan  05/21/2014  Server V1.3     Add config file processing and SGP command/response parser
    ltXuan  05/23/2014  Server V1.4     Cleanup, and add data reception prcessing, data storage file
    ltXuan  06/25/2014  Server V2.05    Simplify the SGP config file
    ltXuan  07/16/2014  Server V2.08    Add over-the-air firmware update feature (i.e. SGP command C,1,<filesize>)
    ltXuan  09/09/2014  Server V3.5     Changed "CSV file format" paramerter from F0 to E0
 *  ltXuan  11/22/2014  Server V3.7.1   1) Take out the .CSV support.  We should have a seperate program to parse logs into .CSV seperately.
*******************************************************************************/
using System;               //For String, Console, Int32, ArgumentException
using System.Threading;     //For Thread
using System.Net;           //For IPAddress, Dns, IPHostEntry
using System.Net.Sockets;   //For TcpListener, Socket.  SocketException
using System.IO;            

namespace TTUServerTest
{
    class TTUServerMain
    {
        public const int SERVER_PORT = 82;

        public const int BUFSIZE = 100;                 //size of IO buffer (for output SGP command and incoming data)
        public const int DATA_BUFSIZE = 8192;          //8KB for both receiving and sending buffer
        public const int CONNECTION_TIMEOUT = 30000;   //30s
        public const int LINGER_STATE_TIMEOUT = 10000; // The socket will linger for 10 seconds after Socket.Close is called.
        public const string _KEEPCONNECTIONALIVETIME = "3C";       //1E=30s  3C=60s


        public const string _CMD_1_LOGIN_OK_FOR_DATA = "1";            //login ok, client can send data
        public const string _CMD_2_LOGIN_OK_FOR_SETUP = "2";           //login ok, but client waits for next command
        public const string _CMD_C5_KEEP_CONNECTION_ALIVE_TIME = "C,5,";   //format: C,5,<hex in minutes>
        public const string _CMD_USERNAME = "USERNAME:";
        public const string _CMD_PASSWORD = "PASSWORD:";
        public const string _CMD_B_FOR_SETTING_PARAM = "B";
        public const string _CMD_C1_FW_UPDATE = "C,1,";                //format: C,1,<filesize in decimal>
        public const string _CMD_C2_READ_PARAM = "C,2,0,";             //format: C,2,0,<paramID>
        public const string _CMD_C3_WRITE_PARAM = "C,3,0,";            //format: C,3,0,<paramID>,<new value>
        public const string _CMD_C0_END_CONNECTION = "C,0";

        public const string _PARAMID_FOR_DATETIME = "2";
        public const string _PARAMID_FOR_TRANSMISSION_INTERVAL = "20";
        public const string _PARAMID_FOR_MEASUREMENT_INTERVAL = "40";
        public const string _PARAMID_FOR_MEASUREMENT_CMD_LIST = "43";
        public const string _PARAMID_FOR_SENSOR_ADDRESS_LIST = "45";
        //public const string _PARAMID_FOR_UNKNOWN = "FF";

        public const string _PARAMID_FOR_FILEFORMAT = "E0";     //file format param ID

        //public const string _RESULT_OK = "1";
        //public const string _RESULT_FAILED = "-1";
        //public const string _COMMENT_CHAR_IN_CONFIG_FILE = "#";

        public static string _dataDir;
        public static string _configDir;
        public static string _deviceDir;
        public static string _defaultConfigFile;
        //public static string _csvFileDir;

        private static string _ttuRootDir;
        private static string _sourceDir;
        private static ILogger consoleLogger;

        /*******************************************************************************/
        // declares the enum
        public enum CmdCode
        {
            CMD_USERNAME,
            CMD_PASSWORD,
            CMD_0,
            CMD_1,
            CMD_2,
            CMD_B,
            CMD_C0,
            CMD_C1,
            CMD_C2,
            CMD_C3,
            CMD_C5,

            CMD_WAIT,
            CMD_NONE,
            CMD_DONE,
            CMD_ERROR,
        }
        /*******************************************************************************/

        public enum RespState
        {

            RESP_USERNAME,
            RESP_PASSWORD,

            RESP_1,
            RESP_FILE,
            RESP_DELAY,
            RESP_R2,
            RESP_R3,
            RESP_R5,

            RESP_NONE,
            RESP_ERROR,
        }

        /*******************************************************************************/
        public enum RESULT_STATUS
        {
            RESP_RCVD_OK,
            RESP_RCVD_CONTINUE,
            RESP_RCVD_ERROR,

        }
        /*******************************************************************************/
        static void Main(string[] args)
        {
            _ttuRootDir = @"C:\TTUTest";
            consoleLogger = new ConsoleLogger();   //Log messages to console
            //ILogger logger = new FileLogger("C:\\TTU\\Data\\Log1.txt");

            //string server = "192.168.1.121"; ;
            int servPort = SERVER_PORT; 
            
            //Server to create a new socket if there is an incoming connection
            TcpListener listener = new TcpListener(IPAddress.Any, servPort);          //Blocking until incoming connection
            //TcpListener listener = new TcpListener(IPAddress.Parse(server), servPort);  //Blocking until incoming connection
            listener.Start();
            consoleLogger.writeText("Server endpoint: " + listener.LocalEndpoint.ToString() + ".  Listening...");

            if (createTTUDirectory())
            {

                //Run forever, accepting and spawning threads to service each connection
                for (; ; )
                {
                    try
                    {
                        //Server's TCPListener to create a new socket for the new connection
                        Socket clntSock = listener.AcceptSocket();  //Block waiting for connection

                        SGPProtocol protocol = new SGPProtocol(clntSock, consoleLogger);
                        Thread thread = new Thread(new ThreadStart(protocol.handleClient));
                        consoleLogger.writeText("Created Thread " + thread.GetHashCode() + ".  This thread is starting...");

                        thread.Start();
                    }
                    catch (System.IO.IOException e)
                    {
                        consoleLogger.writeText("TTU exception handling: " + e.Message);
                    }
                }
                //WONT GET HERE
            }
            else
            {
                consoleLogger.writeText("ERROR: Can't create logging directory and log file");
            }
        }
        /*****************************************************************************/
        static bool createTTUDirectory()
        {
            //string ttuRootDir = @"C:\TTUTest";
            bool ret = true;
            try
            {

                //currentPath = Environment.CurrentDirectory.ToString();
                _sourceDir = _ttuRootDir + @"\TTUSource";
                _dataDir = _sourceDir + @"\Data";
                _configDir = _sourceDir + @"\Config";
                _deviceDir = _sourceDir + @"\Device";
                _defaultConfigFile = _configDir + @"\default.cfg";
                //_csvFileDir = _sourceDir + @"\CSV";

                if (!Directory.Exists(_dataDir))
                {
                    Directory.CreateDirectory(_dataDir);
                }
                if (!Directory.Exists(_configDir))
                {
                    Directory.CreateDirectory(_configDir);
                }
                if (!Directory.Exists(_deviceDir))
                {
                    Directory.CreateDirectory(_deviceDir);
                }
                //if (!Directory.Exists(_csvFileDir))
                //{
                //    Directory.CreateDirectory(_csvFileDir);
                //}
                consoleLogger.writeText("Data directory: " + _dataDir
                               + "\nConfig directory: " + _configDir
                               + "\nDevice directory: " + _deviceDir);

                if (!File.Exists(_defaultConfigFile))
                {
                    consoleLogger.writeText("ERROR: No default config file in: " + _defaultConfigFile);
                    return false;
                }

            
            //}catch
            //{
            //    ret = false;
            //    //Catch all exceptions
            //    logger.writeText("Exception in createTTUDirectory().");
            //}
            }catch(System.Security.SecurityException se){
                consoleLogger.writeText("Securitiy Exception" + se.GetHashCode()); 
                ret=false;
            }catch(DirectoryNotFoundException de){
                consoleLogger.writeText("DirectoryNotFounnd Exception" + de.GetHashCode()); 
                ret=false;
            }catch(IOException ie){
                consoleLogger.writeText("IO Exception" + ie.GetHashCode()); 
                ret=false;
            }

            return ret;
        }
    }
}
