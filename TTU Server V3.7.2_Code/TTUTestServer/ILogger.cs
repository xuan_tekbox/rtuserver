﻿using System;               //For String
using System.Collections;   //For ArrayList

namespace TTUServerTest
{
    public interface ILogger
    {
        void writeEntry(ArrayList entry);   //Write list of lines
        void writeText(String text);      //Write single line
        void writeClose();
    }
}
